import Vue from 'vue'
import Router from 'vue-router'
import Home from './App.vue'
import Easy from './views/Easy.vue'
import Medium from './views/Medium.vue'
import Hard from './views/Hard.vue'

Vue.use(Router)

// Set up routes
export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/easy',
      name: 'easy',
      component: Easy
    },
    {
      path: '/medium',
      name: 'medium',
      component: Medium
    },
    {
      path: '/hard',
      name: 'Hard',
      component: Hard
    }
  ]
})